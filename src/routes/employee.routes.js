import { Router } from "express";

import {
  getEmployees,
  createNewEmployee,
  getEmployeeById,
  deleteEmployeeById,
  getTotalEmployees,
  updateEmployeeById,
} from "../controllers/employees.controller";

const router = Router();

router.get("/employees", getEmployees);

router.post("/employee", createNewEmployee);

router.get("/employees/count", getTotalEmployees);

router.get("/employee/:id", getEmployeeById);

router.delete("/employee/:id", deleteEmployeeById);

router.put("/employee/:id", updateEmployeeById);

export default router;
