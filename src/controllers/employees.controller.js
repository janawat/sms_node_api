import { getConnection, queryEmp, sql } from "../database";

export const getEmployees = async (req, res) => {
    try {
        const pool = await getConnection();
        const result = await pool.request().query(queryEmp.getAllEmployees);
        res.json(result.recordset);
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
};

export const createNewEmployee = async (req, res) => {
    //const { EMP_ID,FName,LName,EMail,Phone } = req.body;

    const emp = {...req.body}
 
    // validating
    if (emp.EMP_ID == null || emp.FName == null|| emp.LName == null || 
        emp.EMP_ID == "" || emp.FName == ""|| emp.LName == "") {
        return res.status(400).json({ msg: "Bad Request. Please fill all fields" });
    }

    try {
        const pool = await getConnection();

        await pool
            .request()             
            
            .input("EMP_ID", sql.VarChar, emp.EMP_ID)
            .input("FName", sql.VarChar, emp.FName)
            .input("LName", sql.VarChar, emp.LName)
            .input("EMail", sql.VarChar, emp.EMail)
            .input("Phone", sql.VarChar, emp.Phone) 
               
            .query(queryEmp.addNewEmployee);

        //res.json({ EMP_ID,FName,LName,EMail,Phone});
        res.status(201).json(emp);
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
};

export const getEmployeeById = async (req, res) => {
    try {
        const pool = await getConnection();

        const result = await pool
            .request()
            .input("id", req.params.id)
            .query(queryEmp.getEmployeeById);
        return res.json(result.recordset[0]);
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
};

export const deleteEmployeeById = async (req, res) => {
    try {
        const pool = await getConnection();

        const result = await pool
            .request()
            .input("id", req.params.id)
            .query(queryEmp.deleteEmployeeById);

        if (result.rowsAffected[0] === 0) return res.sendStatus(404);

        return res.sendStatus(204);
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
};

export const getTotalEmployees = async (req, res) => {
    const pool = await getConnection();

    const result = await pool.request().query(queryEmp.getTotalEmployees);
    console.log(result);
    res.json(result.recordset[0][""]);
};

export const updateEmployeeById = async (req, res) => {
    
    const { EMP_ID,FName,LName,EMail,Phone } = req.body;

    // validating
    if (EMP_ID == null || FName == null || LName == null) {
        return res.status(400).json({ msg: "Bad Request. Please fill all fields" });
    }

    try {
        const pool = await getConnection();
        await pool
            .request()
            
            .input("FName", sql.VarChar, FName)
            .input("LName", sql.VarChar, LName)
            .input("EMail", sql.VarChar, EMail)
            .input("Phone", sql.VarChar, Phone) 
            .input("EMP_ID", req.params.id)

            .query(queryEmp.updateEmployeeById);
        res.json({ EMP_ID,FName,LName,EMail,Phone });
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
};
