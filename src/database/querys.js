export const querys = {
  getAllProducts: "SELECT TOP(500) * FROM [Test_SecuredDb].[dbo].[Products]",
  getProducById: "SELECT * FROM Products Where Id = @Id",
  addNewProduct: "INSERT INTO [Test_SecuredDb].[dbo].[Products] (name, description, quantity) VALUES (@name,@description,@quantity);",
  deleteProduct: "DELETE FROM [Test_SecuredDb].[dbo].[Products] WHERE Id= @Id",
  getTotalProducts: "SELECT COUNT(*) FROM Test_SecuredDb.dbo.Products",
  updateProductById: "UPDATE [Test_SecuredDb].[dbo].[Products] SET Name = @name, Description = @description, Quantity = @quantity WHERE Id = @id",
  
};
